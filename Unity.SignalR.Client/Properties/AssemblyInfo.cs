﻿// Copyright (c) Microsoft Open Technologies, Inc. All rights reserved. See License.md in the project root for license information.

using System.Reflection;
using System;
using System.Resources;
using System.Runtime.InteropServices;


[assembly: AssemblyTitle("TrueSight.SignalR.Client.Unity")]
[assembly: AssemblyDescription("SignalR v2.x Client for Unity3D")]

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyCompany("TrueSight Studio")]
[assembly: AssemblyCopyright("Copyright © TrueSight Studio 2017")]
[assembly: AssemblyProduct("TrueSight.SignalR.Client.Unity")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: CLSCompliant(false)]
[assembly: NeutralResourcesLanguage("en-US")]

[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]