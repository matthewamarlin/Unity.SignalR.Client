# Unity.SignalR.Client

## Overview

SignalR v2.x Client backport for Unity (based on the Official SignalR 2.0.0 tag)
Built against "Unity 3.5 .net subset for base class libraries"

Source forked from:
https://github.com/oising/Nivot.SignalR.Client.Net35
https://github.com/SignalR/SignalR

# License: 

MIT